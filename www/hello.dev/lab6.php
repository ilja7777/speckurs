<?php

header('Content-Type: text/html; charset=utf-8');

$act = $_GET['n'];
$obj = new LAB6((int)$act);
?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
<?php

/**
* 
*/
class LAB6
{
	
	function __construct($nlab)
	{
		$this->content='';
		switch ($nlab) {
			case 1: $this->lab1();
				# code...
				break;
			case 2:$this->lab2();
				# code...
				break;
			case 3:$this->lab3();
				# code...
				break;
			case 4:$this->lab4();
				# code...;
				break;
			case 5:$this->lab5();
				# code...
				break;
			default:
				# code...
				break;
		}
	}
	public function lab1(){
		$filename="file.txt";
		if(isset($_POST['history']) && $_POST['history']!=''){
			$text = $_POST['history']."\n";
			$handle = fopen($filename, 'a') or die("Не могу открыть файл ($filename)");
	   		fwrite($handle, $text);
	   		fclose($handle);
		}
		$f = file($filename);
		
		if ($f){
			$this->content.="<fieldset><legend>Вся история: </legend>";
			for ($i=0; $i < count($f); $i++) { 
				$this->content.="<div style='border: 1px solid green; margin: 5px;'>".$f[$i]."</div>";
			}
			$this->content."</fieldset>";
		}

	}
	public function lab2(){
		if (isset($_POST['name']))
		{
		$name=trim($_POST['name']);
		setcookie('user_name',$name,time() + (86400 * 5));
		setcookie('visit_date',date('d/m/Y'),time() + (86400 * 5));
		setcookie('visit_time',date("H:i"),time() + (86400 * 5));
		$this->content.="Вас успешно запомнили";
		} else {
			$date=$_COOKIE['visit_date'];
			$time=$_COOKIE['visit_time'];
			setcookie('visit_date',date('d/m/Y'),time() + (86400 * 5));
			setcookie('visit_time',date("H:i"),time() + (86400 * 5));
			echo 'Привет, '.htmlspecialchars($_COOKIE['user_name']).'! Последний визит: '.$date.' в '.$time;
			
		}
	}
	public function lab3(){
		echo "Работу выполнил Гриньков Илья Юрьевич, студент Адыгейского государственного университета, группы 4ИС, по предмету: Web-Программирование, декабрь 2018 г.<br>";
		include("count.php");
		//$this->content.=$text;
		
	}
	public function lab4(){
		function iscorrectLogin($log,$pass){
			$filename="users.txt";
			$result = @file_get_contents($filename);
			$obj = @json_decode($result);
			return (isset($obj->{$log}) && $obj->{$log}->{'pass'}==$pass);
		}
		if (isset($_POST['login'])){

			$login=trim($_POST['login']);
			$pass=trim($_POST['pass']);


			if(iscorrectLogin($login,$pass)){
				if(!isset($_POST['remember'])){
					setcookie('name',$login);
					setcookie('pass',$pass);
				} else {
					setcookie('name',$login,time()+60*60*24*365);
					setcookie('pass',$pass,time()+60*60*24*365);
					setcookie('remember','t',time()+60*60*24*365);
				}
				header('Location: http://localhost/lab6.php?n=4');
			} else {
				$this->content.='Неверны логин или пароль';
			}
		}

		if (isset($_POST['logout']))
		{
			setcookie('name','',time()-10000);
			setcookie('pass','',time()-3000);
			setcookie('remember','',time()-3000);
			header('Location: http://localhost/lab6.php');
			
		}
		if (isset($_COOKIE['name']) && isset($_COOKIE['pass']) && iscorrectLogin($_COOKIE['name'], $_COOKIE['pass'] ) ) {
			if(isset($_COOKIE['remember'])){
				setcookie('name',$_COOKIE['name'],time()+60*60*24*365);
				setcookie('pass',$_COOKIE['pass'],time()+60*60*24*365);
			}

			$this->content.="<h2>Закрытая часть</h2>";
			$this->content.="<form action='' method='POST'>Здравствуйте, <b>".htmlspecialchars($_COOKIE['name'])."</b>!<br><input type='submit' name='logout' value='Выйти'></form>";
		}
	}
	public function lab5(){
		$filename="products.txt";
		$result = @file_get_contents($filename);
		$result = @json_decode($result);
		@session_start();
		if(!isset($_SESSION['cart'])){
			$_SESSION['cart'] = array();
		}
		$cart =$_SESSION['cart'];
		if(isset($_POST['id'])){
			$id = (int)$_POST['id'];
			$id = (string)$id;
			if( isset($result->{$id})){
				if(isset($cart[$id])){
					$cart[$id]['count']+=1;
				} else {
					$cart[$id] = array();
					$cart[$id]['count'] = 1;
				}
				$_SESSION['cart'] = $cart;
			}
		}
		$this->content.='<h3>Интернет магазин "Электроникc"</h3><hr/>';
		$this->content.='<h4>Ваша корзина</h4>';
		if(isset($_GET['act']) && $_GET['act']=='cart'){
			$summ = 0;
			$tb = '';
			if(isset($_GET['type']) && $_GET['type']=='del'){
				$id = (int)$_GET['id'];
				unset($cart[$id]);

			}
			foreach ($cart as $id => $value) {
				$tb.='<tr>';
				$tb.='<td>'.$result->{$id}->{'title'}.'</td>';
				$tb.='<td>'.$cart[$id]['count'].'</td>';
				$tb.='<td>'.$result->{$id}->{'price'}.'</td>';
				$tb.='<td><a href="?n=5&act=cart&id='.$id.'&type=del">Убрать</a></td>';
				$summ+=$cart[$id]['count']*$result->{$id}->{'price'};
				$tb.='</tr>';
			}
			$this->content.='<table border="1"><tr><th>Товар</th><th>Количество</th><th>Цена</th><th></th></tr>'.$tb.'<tr><td colspan="2">Итого </td><td>'.$summ.'</td><td></td><tr></table>';
			$this->content.='<a href="?n=5">Список товаров</a>';
			$_SESSION['cart'] = $cart;
			return;
		}

		

		$summ = 0;
		$coun = 0;
		foreach ($cart as $id => $value) {
			$summ+=$value['count']*$result->{$id}->{'price'};
			$coun+=$value['count'];
			# code...
		}
		$this->content.='<div>Товаров '.$coun.'</div>';
		$this->content.='<div>Стоимость '.$summ.'</div>';
		$this->content.='<div><a href="?n=5&act=cart">Подробнее...</a></div><hr/>';

		foreach ($result as $id => $value) {
			$this->content.='<div><b>'.$value->title.'</b>';
			$this->content.=' '.$value->price.' руб. ';
			$this->content.='<form action="?n=5" method="POST"><input type="hidden" name="id" value="'.$id.'"/><button type="submit">В корзину</button></form></div>';
			# code...
		}
		$_SESSION['cart'] = $cart;


		//var_dump($result);
		
	}

}


if(!isset($_GET['n'])){
?>
	<fieldset>
		<legend>Задание 1. 
</legend>
<?php
	$filename="file.txt";
	$f = @file($filename);
	$mess = 'Вы будете первым';
	if (!$f){
		$handle = @fopen($filename, "a+");
		@fclose($handle);
		$f = @file($filename);
	} else {
		$mess = "Последнее предложение истории: ".$f[count($f)-2];
	}
	echo $mess;
?>
	<form method="POST" action="?n=1">
		<br>
		<textarea name='history' cols="35" rows="4" placeholder='Введите своё продолжение истории'></textarea>
		<input type="submit" name="add" value="Добавить" />
	</form>
	</fieldset>
	<fieldset>
		<legend><a href="?n=2">Задание 2. </a></legend>
<?php
if (!isset($_COOKIE['user_name']))
{
?>
<form action='?n=2' method="POST">
	<label for="name">Это Ваш первый визит. Введите Ваше имя:</label><br>
	<input type="text" maxlength="30" name="name">
	<input type="submit" value="Запомнить меня">
</form>
<?php
}
?>
	</fieldset>
	<fieldset>
		<legend><a href="?n=3">Задание 3. </a></legend>
	</fieldset>

	<fieldset>
		<legend>Задание 4.</legend>
		<form action="?n=4" method="POST">
			<h2>Авторизация на сайте</h2>
			<div>
				<label style="margin: 3px; font-size:20; font-weight:bold; width: 60px; display: inline-block;">Логин: </label>
				<input type="text" name="login">
			</div>
			<div >
				<label style="margin: 3px; font-size:20; font-weight:bold; width: 60px; display: inline-block;">Пароль:</label>
				<input type="password" name="pass">
			</div>
			<div>
				
				<input type="checkbox" name="remember" checked />
				<label style="font-size:20; font-weight:bold;" for="remember">Запомнить</label>
			</div>
			<div>
				<input type="submit" value="Войти">
			</div>
		</form>
	</fieldset>

	<fieldset>
		<legend><a href="?n=5">Задание 5.</a></legend>
	</fieldset>


<?php
} else {
	
	
	echo $obj->content;

}


?>

</body>
</html>